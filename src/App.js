import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import CartButton from './cart-button/CartButton';

function App() {
  return (
    <div className="App">
      <Box sx={{ width: '100%', maxWidth: 1000 }}>
        <Typography variant="h2" gutterBottom>
          Boton de carrito de compras
        </Typography>
        <Typography variant="h6" gutterBottom>
          Este boton de carrito de compras (
          <CartButton />) puede ser usado desde cualquier app usando Module
          Federation. Tambien esta habilitado para escuchar el evento
          'cartAction' que indica cuando se agrego o removio un item del carrito
        </Typography>
        <Typography variant="body2">
          Puedes probarlo abriendo la consola y ejecutando (No funciona en
          Stackblitz)
        </Typography>
        <code>
          {`
                    document.dispatchEvent(new CustomEvent('cartAction', { detail: 'add' }))
                        `}
        </code>
      </Box>
    </div>
  );
}

export default App;
