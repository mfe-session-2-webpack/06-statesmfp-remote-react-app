import React, { useEffect, useState } from 'react';
import IconButton from '@mui/material/IconButton';

import Badge from '@mui/material/Badge';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

export default function CartButton() {
  const [items, setItems] = useState(0);

  useEffect(() => {
    const handleEvent = (event) => {
      event.detail ? setItems(items + 1) : setItems(Math.max(items - 1, 0));
    };

    document.addEventListener('cartAction', handleEvent);

    return () => {
      document.removeEventListener('cartAction', handleEvent);
    };
  }, [items]);

  return (
    <IconButton aria-label="cart">
      <Badge badgeContent={items} color="success">
        <ShoppingCartOutlinedIcon color="action" />
      </Badge>
    </IconButton>
  );
}
